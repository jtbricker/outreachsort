#!/usr/bin/python

import sys

########################################
#
#   Read Command Arguments, etc...
#
def read_cargs():
	sapp_fname = sys.argv[1]
	tapp_fname = sys.argv[2]
	sapp_file = open(sapp_fname)
	tapp_file = open(tapp_fname)
	sfile_lines = sapp_file.readlines()
	tfile_lines = tapp_file.readlines()
	return (sfile_lines,tfile_lines)

#########################################
#
#  Create Array of Header Key Values
#

def extract_key(file_inlines):
	return file_inlines[0].split('\t')

#########################################
#
#  Create Array of Applications
#

def extract_apps(file_inlines):
	apps = []
	for i in range(1,len(file_inlines)):
		app = file_inlines[i].split('\t')
		apps.append(app)
	return apps

#########################################
#
#  Create a dictionary from header keys
#  to app parameter values
#

def make_dic(apps, keys, i):
	app = apps[i]
	dic = {}
	for n in range(0,len(keys)):
		dic[keys[n]] = app[n]
	return dic

#################################################
#
#  Make an array of the dictionaries of each
#  application
#

def make_all_dics(apps, keys):
	mdic = []
	k = 0
	while ( k < len(apps)):
		dic = make_dic(apps, keys, k)
		mdic.append(dic)
		k = k+1
	return mdic

#################################################
#
#  Print summary information for a single 
#  student
#

def print_ssummary(sds, idn):
	fn = skey[1]   #first name
	ma = skey[2]   #monday avail
	ln = skey[3]   #last name
	pn = skey[4]   #phone number
	ea = skey[5]   #email address
	da = skey[6]   #driving avail?
	ta = skey[10]   #tuesday avail
	wa = skey[11]   #wednesday avail
	ra = skey[12]   #thursday avail
	fa = skey[13]  #friday avail
	pp = skey[7]  #prefered parter?
	pl = skey[8]  #p.partner last name
	pf = skey[9]  #p.partner first name

	print "\n\n***********************************************"
	print "%s%s %s" %("\t\t",sds[idn][fn],sds[idn][ln]) 
	print "***********************************************"
	print "%s%s" %("Phone Number: ", sds[idn][pn])
	print "%s%s" %("Email Address: ", sds[idn][ea])
	print "%s%s" %("Can Drive: ", sds[idn][da])
	print "%s%s" %("Preferred Partner: ", sds[idn][pp])
	print "%s%s %s" %("Partner Name: ", sds[idn][pf], sds[idn][pl])
	print "\n============  Availability  ===================\n"
	print "%s%s" %("Monday: ", sds[idn][ma])
	print "%s%s" %("Tuesday: ", sds[idn][ta])
	print "%s%s" %("Wednesday: ", sds[idn][wa])
	print "%s%s" %("Thursday: ", sds[idn][ra])
	print "%s%s" %("Friday: ", sds[idn][fa])

#################################################
#
#  Print summary information for a single 
#  teacher
#

def print_tsummary(tcs, idn):
	fn = tkey[1]  #first name
	ln = tkey[2]  #last name
	sn = tkey[3]  #school name
	gd = tkey[4]  #grade
	ea = tkey[5]  #email address
	pn = tkey[6]  #phone number
	ma = tkey[7]  #monday avail
	ta = tkey[8]  #tueday avail
	wa = tkey[9]  #wednesday avail
	ra = tkey[10] #thursday avail
	fa = tkey[11] #friday avail
	fp = tkey[12] #frequency pref
	oc = tkey[13] #other comments

	print "\n\n************************************************"
	print "%s%s %s" %("\t\t",tcs[idn][fn], tcs[idn][ln])
	print "************************************************"
	print "%s%s" %("School: ", tcs[idn][sn])
	print "%s%s" %("Email Address: ", tcs[idn][ea])
	print "%s%s" %("Phone Number: ", tcs[idn][pn])
	print "\n================Availability===================\n"
	print "%s%s" %("Monday: ", tcs[idn][ma])
	print "%s%s" %("Tuesday: ", tcs[idn][ta])
	print "%s%s" %("Wednesday: ", tcs[idn][wa])
	print "%s%s" %("Thursday: ", tcs[idn][ra])
	print "%s%s\n" %("Friday: ", tcs[idn][fa])
	print "############### OTHER COMMENTS ##################"
	print tcs[idn][oc]



#################################################
#
#  Print Summaries for all students
#

def print_allsum(sds,c):
	k=0
	while (k < len(sds)):
		if c == 's':
			print_ssummary(sds,k)
		if c == 't':
			print_tsummary(sds,k)
		print "\n@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
		k = k+1


##################################################
#
#  Replace availability strings with
#  arrays
#

def format_availability(student):
	days = []
	for h in range(0,5):
		periods = []
		for k in range(0,9):
			periods.append('N')
		days.append(periods)
	
	day = 0
	for p in [2,10,11,12,13]:	 
		for n in range(1,10):
			te = "%s %d" %("Period",n) 

			if student[skey[p]].find(te) != -1:
				days[day][n-1] = 'Y'
		day = day + 1
				
	return days


##################################################
#
#  Main Method
#

(sfile_lines, tfile_lines) = read_cargs()
skey =  extract_key(sfile_lines)
tkey =  extract_key(tfile_lines)
sapps =  extract_apps(sfile_lines)
tapps =  extract_apps(tfile_lines)
students = make_all_dics(sapps, skey)
teachers = make_all_dics(tapps, tkey)

print_allsum(students,'s')
#print_allsum(teachers,'t')

sk = students[0]
print format_availability(sk)



